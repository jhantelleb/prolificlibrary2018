## Prolific Library Coding Test
#### Description of PR: 
The app helps in tracking the books in the Prolific library. 3 types of users would be able to use the app.

###Users:
1. Admin - responsible for adding, deleting and updating the checkout status of the books in the library.
2. Users with login - using company provided email and password, they are able to log into the app, search and checkout books.
3. Users without login - these are users can freely search what's in the Prolific library but are not able to checkout books.

####With this, there are 3 types of flow.
####Users without login flow
- When the user opens the app for the first time, they are presented with a list of books. Users can search the library using the search bar or the localized index. The books can search by Title, Author, or Categories.
- When a user selects a book from the list, a detail view will be presented displaying more details about the book.
- At the bottom of the screen, another tab bar is available asking the user to login.
- When the user taps the login button from the tab bar, they can either log in immediately or log in later.

####Users with login flow
- When a non-admin user is logged in, they are presented with the same view listing the books. When they select a book that has not been checked out, they can easily do so by tapping the checkout button. There's no need to type in the name since the app already knows who the logged in user.
- A share button is also available so users can share the link of the book details to their friends via Twitter and Facebook.
- The second tab bar button is now called Account. When the user presses this, they'd be able to see their name and also log out of the app.

####Admin flow
- Here's the fun part (I HAVE THE POWEEEER! - well almost). When logged in as an admin/librarian, an add bar button item will be available. 


###Admin Scenes
####Adding books
- Tapping the add bar presents a new view that enables the admin to add up to 5 books at once.
- Alert controllers are displayed when you forget to add an entry to a required field. Also when you want to leave the scene while there are unsaved changes.

####In the book detail view
- The admins can delete a book and checkout the book for themselves and even for someone else. 

####Dashboard
- The second tab is now called the Dashboard.
- The admin can see the number of checked out boooks and available books
- Here, the admin can also delete all the books.
- The common user info (name, logout) is also available.

###Other features of the app
Network reachability feature lets users know they need to connect to the internet to use the app.

###Features not included in the app
I decided not to add a check-in feature to check in the books. As mentioned in the description, the app is supposed to help track the books in the library. I think it's safer for the user to physically check in by surrendering the book in return kiosks (like the public libraries have).


###Architecture
I used the Clean Swift Architecture (MVP) to write the app. Due to the nature of the architecture, the SOLID principles are highly emphasized, making the app easily scalable and components reusable.

###Future Improvements
- If I had more time, I would add some unit tests and improve the UI.
- How the user model is handled (currently the users are stored in User Defaults)
- Improved dashboard for the admin

###Challenges
Implementing the different users of the app


### List of Users for testing
- Use the email and password to login
- [ {
 "uuid": "0001",
 "displayName": "Librarian",
 "email": "test",
 "password": "testing",
 "admin": true
 },
 
 - {
  "uuid": "0002",
  "displayName": "Fitzgerald",
  "email": "senectus@prolific.com",
  "password": "testing",
  "admin": false
  },

- {
  "uuid": "0003",
  "displayName": "Lester",
  "email": "pede@prolific.com",
  "password": "testing",
  "admin": false
  },
 
 - {
  "uuid": "0004",
  "displayName": "Chadwick",
  "email": "diam@prolific.com",
  "password": "testing",
  "admin": false
  }
]


For any questions, please email me @ jhantelle.belleza@gmail.com


